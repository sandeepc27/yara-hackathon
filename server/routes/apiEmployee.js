var express = require('express');
const { Sequelize } = require('sequelize');
var router = express.Router();

const {getEmployee , getEmployeeById } = require('../service/Database')

router.get('/:id', async ( req , res ) => {
    const id = req.params.id;
    const employee = await getEmployeeById(id);
    res.send(employee);
});

module.exports = router;