const { Sequelize , DataTypes } = require('sequelize');

//For Postgres - database: yarahackathon , username: postgres, password: '123456', dialect: postgres

const sequelize = new Sequelize (
    'postgres' ,
    'postgres' ,
    '123456' , {
        host : 'localhost' ,
        dialect : 'postgres'
});

const Hackathon = sequelize.define('Hackathon',{
    hackathonid: {
        type: DataTypes.STRING,
        // primaryKey: true,
        // allowNull: false
    },
    hackathonname: {
        type: DataTypes.STRING
    },
    poster: {
        type: DataTypes.STRING
    },
    hackathondate: {
        type: DataTypes.DATE
    },
    venue: {
        type: DataTypes.STRING
    },
    prize: {
        type: DataTypes.STRING
    },
    winner: {
        type: DataTypes.STRING
    },
    runner: {
        type: DataTypes.STRING
    }
},{
    tableName:'hackathon'
});

const Team = sequelize.define('Team',{
    teamid: {
        type: DataTypes.STRING
    },
    teamname: {
        type: DataTypes.STRING
    },
    members:{
        type: DataTypes.JSON
    },
    problemstatement: {
        type: DataTypes.STRING
    },
    createddate: {
        type: DataTypes.DATE
    },
    updateddate: {
        type: DataTypes.DATE
    }
},{
    tableName: 'teamdetails'
});

const Employee = sequelize.define('Employee',{
    employeeid: {
        type: DataTypes.STRING
    },
    employeename: {
        type: DataTypes.STRING
    },
    employeepassword: {
        type: DataTypes.STRING
    },
    employeerole: {
        type: DataTypes.STRING
    }
},{
    tableName: 'employee'
});

const Vote = sequelize.define('Vote', {
    employeeid: {
        type: DataTypes.STRING
    },
    teamid: {
        type: DataTypes.STRING
    }
})


module.exports = {Hackathon,Team,Employee,Vote}