const { Pool , Client } = require('pg')

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: '123456',
    port: 5432
})

//Display all details code of Hackathon , Teams and Employees

const getHackathon = function(){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM hackathon" , function( error , results , fields ) {
            if(error){
                resolve([]);
                throw error;
            }else{
                resolve(results.rows);
            }
        })
    })
}

const getEmployee = function(){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM employee" , function( error , results , fields ) {
            if(error){
                resolve([]);
                throw error;
            }else{
                resolve(results.rows);
            }
        })
    })
}

const getTeam = function(){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM teamdetails" , function( error , results , fields ) {
            if(error){
                resolve([]);
                throw error;
            }else{
                resolve(results.rows);
            }
        })
    })
}

const getVotes = function(){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM vote" , function( error , results , fields ) {
            if(error){
                resolve([]);
                throw error;
            }else{
                resolve(results.rows);
            }
        })
    })
}

// Get details by date

const getOngoingHackathon = function(){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM hackathon where hackathondate>=current_date" , function( error , results , fields ) {
            if(error){
                resolve([]);
                throw error;
            }else{
                resolve(results.rows);
            }
        })
    })
}

const getExpiredHackathon = function(){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM hackathon where hackathondate<current_date" , function( error , results , fields ) {
            if(error){
                resolve([]);
                throw error;
            }else{
                resolve(results.rows);
            }
        })
    })
}

//Get individual record in the table

const getHackathonById = function(id){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM hackathon WHERE hackathonid='"+id+"'",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows[0])
                }
                else{
                resolve({})
                }
            }
        })
    })
}

const getEmployeeById = function(id){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM employee WHERE empid='"+id+"'",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows[0])
                }
                else{
                resolve({})
                }
            }
        })
    })
}

const getTeamById = function(id){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM teamdetails WHERE teamid='"+id+"'",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows[0])
                }
                else{
                resolve({})
                }
            }
        })
    })
}


const getTeamByHackthon = function(id){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM teamdetails WHERE hackathonid='"+id+"'",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows)
                }
                else{
                resolve({})
                }
            }
        })
    })
}

//vote count

const getVotesCount = function(){
    return new Promise(( resolve , reject ) => {
        pool.query("select team.teamid,team.teamname,team.problem,team.members,vot.count from teamdetails team join (SELECT teamid,COUNT(*) as count FROM vote group by teamid order by COUNT(*) desc) vot on(team.teamid = vot.teamid)",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows)
                }
                else{
                resolve({})
                }
            }
        })
    })
}

// count

const getCountHackathon = function(){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT COUNT(*) FROM hackathon" , function( error , results , fields ) {
            if(error){
                resolve([]);
                throw error;
            }else{
                resolve(results.rows[0]);
            }
        })
    })
}

const getTeamCountByHackthon = function(id){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT COUNT(*) FROM teamdetails WHERE hackathonid='"+id+"'",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows[0])
                }
                else{
                resolve({})
                }
            }
        })
    })
}

//id generation
const getNextHackathonId = async() =>{
    var counts = await getCountHackathon();
    var len = counts.count;
    let inc = parseInt(len)+1;
    if(len < 1)
       return "hackathon_"+1; 
    return "hackathon_"+inc; 
}


const getNextTeamId = async(hackathonid) =>{
    let counts = await getTeamCountByHackthon(hackathonid);
    var len = counts.count;
    let inc = parseInt(len)+1;
    if(len < 1)
       return hackathonid+"_team_"+1; 
    return hackathonid+"_team_"+inc; 
}


// add record

const addHackathon = async function(item){
    item.hackathonid= await getNextHackathonId()
    const text = "INSERT INTO hackathon(hackathonid , hackathonname , poster , hackathondate , venue , prize , winner , runner) VALUES($1 , $2 , $3 , to_date($4,'yyyy-mm-dd') , $5 , $6 , $7 , $8) RETURNING *";
    const values = [item.hackathonid , item.hackathonname , item.poster , item.hackathondate , item.venue , item.prize , item.winner , item.runner];
    try{
        return pool.query(text , values)
    }catch( err ){
        console.log(err.stack)
    }
}

const addTeam = async function(item){
    item.teamid = await getNextTeamId(item.hackathonid);
    return new Promise(( resolve , reject ) => {
        pool.query("INSERT INTO teamdetails VALUES('"+item.teamid+"' , '"+item.hackathonid+"' , '"+item.teamname+"' , '"+JSON.stringify(item.members)+"' , '"+item.problem+"' , current_date ) RETURNING *",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows[0])
                }
                else{
                resolve({})
                }
            }
        })
    })
}

const addVote = function( empid , teamid ){
    const text = 'INSERT INTO vote(empid , teamid ) VALUES($1 , $2 ) RETURNING *';
    const values = [ empid , teamid ];
    try{
        return pool.query(text , values)
    }catch( err ){
        console.log(err.stack)
    }
}

//search for member 

   const getVoteBySearch = function(id){
    return new Promise(( resolve , reject ) => {
        pool.query("SELECT * FROM vote WHERE empid='"+id+"'",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows)
                }
                else{
                resolve({})
                }
            }
        })
    })
}


const getEmployeeBySearch = function(field,text){
    return new Promise((resolve, reject)=> {
      var hackathonid = field;
      var value = text;
      var record = {};
      var sql = "SELECT * FROM teamdetails WHERE members::name LIKE "+"'%"+value+"%' and hackathonid='"+hackathonid+"'";
      console.log(sql);
      pool.query(sql, function (error, results, fields){
       if(error) {
           resolve([]);
           throw error;
       }else{
           resolve(results.rows);
       }
      })
    });
   }

// Delete a team

const deleteTeam = function(id){
    return new Promise(( resolve , reject ) => {
        pool.query("DELETE FROM teamdetails where teamid='"+id+"'",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows[0])
                }
                else{
                resolve({})
                }
            }
        })
    })
}


const doTruncate = function(id){
    return new Promise(( resolve , reject ) => {
        pool.query("TRUNCATE TABLE vote",function( error , results , fields ){
            if(error){
                resolve([]);
                throw error;
            }else{
                if(results.rows.length > 0){
                    resolve(results.rows[0])
                }
                else{
                resolve({})
                }
            }
        })
    })
}

// Update a Record 

const updateHackathon = function(items){
    const sql = "UPDATE hackathon set hackathonname=$1, poster=$2, hackathondate=to_date($3,'yyyy-mm-dd'), venue=$4, prize=$5, winner=$6, runner=$7 WHERE hackathonid = $8";
    const values = [items.hackathonname , items.poster , items.hackathondate , items.venue , items.prize , items.winner , items.runner , items.hackathonid];
    try {
      return pool.query(sql, values)
      console.log(res.rows[0])
    } catch (err) {
      console.log(err.stack)
    }
};

const updateTeam = function(items){
    const sql = "UPDATE teamdetails set hackathonid=$1, teamname=$2, members=$3, problem=$4 , updated= current_date WHERE teamid = $5";
    const values = [items.hackathonid , items.teamname , JSON.stringify(items.members) , items.problem , items.teamid];
    try {
      return pool.query(sql, values)
    } catch (err) {
      console.log(err.stack)
    }
};

module.exports= {getCountHackathon , getHackathon , getOngoingHackathon , getExpiredHackathon , getHackathonById , getEmployee , getEmployeeById , getTeam , getTeamById , getTeamByHackthon , getVotes , getVotesCount , addHackathon , addTeam , addVote , getVoteBySearch , getEmployeeBySearch , deleteTeam , updateHackathon , updateTeam , doTruncate}