const apiEndPoint = 'http://localhost:2000/api/employee'

    export const getEmployeeById = async (id) =>{
        return fetch(apiEndPoint + "/"+id, {
          method: 'GET',
              headers: {'Content-Type': 'application/json;charset=utf-8'}
          })
        .then(response => response.json())
        .then(response => { return response; })
        .catch((error) => { console.log(error); });
      }