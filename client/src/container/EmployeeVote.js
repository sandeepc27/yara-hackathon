import "../styles/RegDetailStyles.css";
import { useNavigate } from "react-router-dom";
import React,{Fragment,useState,useEffect} from 'react';
import Hero from "../component/Hero";
import { useParams } from 'react-router';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {addVote,getVoteBySearch} from "../services/apiVote";
import {getTeamByHackthon} from "../services/apiTeam";
import EmployeeNavbar from "../component/EmployeeNavbar";

const EmployeeVote=()=>{
    const navigate = useNavigate();
    const [details,setDetails] = useState([]);
    const hackathonid = useParams();

    const loadDetails = async ()=>{
        setDetails(await getTeamByHackthon(hackathonid.hackathonid));
    }
    
    useEffect(()=>{
        loadDetails();
    },[]);

    //if new vote else display already voted
    const doVote = async(id)  => {
        let a = await getVoteBySearch(hackathonid.id);
        if(a.length == null){
            await addVote(hackathonid.id,id);
            navigate('/AlreadyVoted/'+hackathonid.id);
        }
        else{
            navigate('/AlreadyVoted/'+hackathonid.id);
        }
    }

    //To check whether data is present
    const docheck=()=>
    {
        if(details.length==null)
            navigate('/NoTeamToVote/'+hackathonid.id);
    }

    return(
        <div>
            <Hero
                cName="hero-mid"
                heroImg="https://drive.google.com/uc?export=view&id=1TLmqZ7WuhCDZRsX2gkWa0BcIZTWHY-FG"
            />
            <EmployeeNavbar/>
            <div>
                <center><h1>Vote here...</h1></center>
                <table className="tbl1">
                <thead> 
                    <tr>
                        <th>Team Id</th>
                        <th>Team Name</th>
                        <th>Problem Statement</th>
                        <th>Member Id</th>
                        <th>Member Name</th>
                        <th>Vote</th>
                    </tr>
                </thead>
                <tbody>
                {docheck()}
                    {details.map((item) => {
                        return(
                            <tr key={item.teamid}>
                                <td>{item.teamid}</td>
                                <td>{item.teamname}</td>
                                <td>{item.problem}</td>
                                <td>
                                    <table className="tbl1"><tr></tr>
                                        {item.members.map(member => (
                                            <tr key={member.id}>
                                                <td><center>{member.id}</center></td>
                                            </tr>
                                        ))}
                                    </table>
                                </td>
                                <td>
                                    <table className="tbl1"><tr></tr>
                                        {item.members.map(member => (
                                            <tr key={member.id}>
                                            <td><center>{member.name}</center></td>
                                            </tr>
                                        ))}
                                    </table>
                                </td>
                                <td><button onClick={()=>doVote(item.teamid)}> Vote</button></td>
                            </tr>
                        )
                    }
                )}
            </tbody> 
        </table>        
    </div>
</div>
)
}
export default EmployeeVote

