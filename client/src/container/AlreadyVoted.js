import React, { useState ,useEffect } from 'react';
import '../styles/AlreadyVotedStyles.css';
import EmployeeNavbar from '../component/EmployeeNavbar'
import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router';

export default function AlreadyVoted() {
  const navigate = useNavigate();
  const {id} = useParams();
  const backtoHackathon = ()=>{
    navigate('/Employee/'+id);
  }
    return (
        <div>
             <EmployeeNavbar/>
             <div>
                <div className="hero">
                  <img alt="background" src="https://drive.google.com/uc?export=view&id=1WRYkuL9gZPPLjYfo7eLYuj3nKq1bqQN6"/>
                    <div className="hero-text">
                      <center>
                        <form className="form1">
                          <h2 className="voteText">You Have Voted!!</h2>
                          <button onClick={()=>backtoHackathon()}>Back To Hackathon</button>
                        </form>
                      </center>
                    </div>
                  </div>
                </div>
              </div>
    );
  };