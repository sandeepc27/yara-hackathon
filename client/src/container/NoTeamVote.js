import React, { useState ,useEffect } from 'react';
import '../styles/NoDataStyles.css';
import Navbar from '../component/Navbar'
import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router';
import  {addHackathon} from "../services/apiHackathon"

export default function NoTeamVote() {
  const navigate = useNavigate();
  const backtoAdmin = ()=>{
    navigate('/Admin');
  }
    return (
        <div classname='body'>
             <center><h1>Sorry.. No One has voted yet!!!</h1></center>
            <center><button onClick={()=>backtoAdmin()}>Back To Admin</button></center>
        </div>
    );
  };