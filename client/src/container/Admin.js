import React, { useState,useEffect } from 'react';
import { useNavigate ,useLocation} from "react-router-dom";
import Navbar from '../component/Navbar';
import Hero from '../component/Hero';
import Tab from '../component/Tab';

export default function Admin() {
  const location = useLocation();
        return (
            <div>
              <Navbar/>
              <Hero
                  cName="hero"
                  heroImg="https://drive.google.com/uc?export=view&id=1TLmqZ7WuhCDZRsX2gkWa0BcIZTWHY-FG"
                  />
              <Tab/>
            </div>
          );
        
 
}
 